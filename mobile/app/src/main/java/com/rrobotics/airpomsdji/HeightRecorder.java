package com.rrobotics.airpomsdji;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.instacart.library.truetime.TrueTime;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import dji.common.flightcontroller.FlightControllerState;
import dji.common.flightcontroller.LocationCoordinate3D;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.products.Aircraft;

public class HeightRecorder extends AsyncTask<Void, Void, Void> {
    private Context context;
    private Aircraft aircraft;
    public boolean run = false;

    public HeightRecorder (Context ctx, Aircraft air) {
        context = ctx;
        aircraft = air;
    }

    protected Void doInBackground(Void... params) {
        run = true;
        Log.d("AirPoMS_DJI", "Saver: initializing NTP");
        try {
            TrueTime.build().withNtpHost("pool.ntp.org").initialize();
            Toast.makeText(context, "NTP init OK", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.d("AirPoMS_DJI", "Exception when trying to get TrueTime", e);
            e.printStackTrace();
        }
        Log.d("AirPoMS_DJI", "Started height saver");
        saveHeight();
        return null;
    }

    private LocationCoordinate3D getLocation() {
        FlightController fc = aircraft.getFlightController();
        FlightControllerState fcs = fc.getState();
        return fcs.getAircraftLocation();
    }

    private void saveHeight() {
        // initialize CSV writer
        CSVWriter writer;
        try {
            File output = new File(Environment.getExternalStorageDirectory(), "airpoms/out.csv");
            String outpath = output.getAbsolutePath();
            Log.d("AirPoMS_DJI", outpath);
            writer = new CSVWriter(new FileWriter(outpath), ',');
        }
        catch (IOException e) {
            Log.d("AirPoMS_DJI", "CSVWriter exception", e);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Error in CSV Writer initialization", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }
        Log.d("AirPoMS_DJI", "CSVWriter initialized");
        // initialize useful vars
        TimeZone UTCtz = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy/hh/mm/ss");
        dateFormat.setTimeZone(UTCtz);
        // loop while run is set to true
        while (run) {
            Log.d("AirPoMS_DJI", "Looping altitude saver");
            // gather data
            LocationCoordinate3D cloc = getLocation();
            float altitude = cloc.getAltitude();
            double latitude = cloc.getLatitude();
            double longitude = cloc.getLongitude();
            // current time
            Date now = TrueTime.now();
            String date = dateFormat.format(now);
            ArrayList<String> blub = new ArrayList<String>();
            for(String s : date.split("/")) {
                blub.add(s);
            }
            blub.add(Double.toString(latitude));
            blub.add(Double.toString(longitude));
            blub.add(Float.toString(altitude));
            String[] toWriter = blub.toArray(new String[blub.size()]);
            writer.writeNext(toWriter);
            // start waiting
            long onFinish = Calendar.getInstance().getTime().getTime();
            Log.d("AirPoMS_DJI", "Loop complete, now waiting.");
            while (Calendar.getInstance().getTime().getTime() - onFinish <= 800) {} // loop over while time is passed
        }
        try {
            writer.close();
        }
        catch (IOException e) {}
        Log.d("AirPoMS_DJI", "Loop exiting");
    }

    public void stopRecording() {
        run = false;
        Log.d("AirPoMS_DJI", "Sent stop signal");
    }

}