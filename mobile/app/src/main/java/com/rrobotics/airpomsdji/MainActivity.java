package com.rrobotics.airpomsdji;

import android.Manifest;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.File;

import dji.common.flightcontroller.FlightControllerState;
import dji.common.flightcontroller.LocationCoordinate3D;
import dji.sdk.products.Aircraft;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.sdkmanager.DJISDKManager;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private HeightRecorder hr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // When the compile and target version is higher than 22, please request the
        // following permissions at runtime to ensure the
        // SDK work well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }

        setContentView(R.layout.activity_main);

    }

    protected void doSHS(View view) {
        // create the project's directory
        File file = new File(Environment.getExternalStorageDirectory(), "airpoms");
        if (file.mkdir()) Toast.makeText(this, "AirPoMS directory created successfully", Toast.LENGTH_SHORT).show();
        Aircraft mProduct = (Aircraft) DJISDKManager.getInstance().getProduct();
        if (mProduct != null) {
            if (hr == null) hr = new HeightRecorder(this, mProduct);
            if (hr.run) {
                hr.stopRecording();
                Toast.makeText(getApplicationContext(), "Stopping recording", Toast.LENGTH_SHORT).show();
            }
            else {
                hr.execute();
                Toast.makeText(getApplicationContext(), "Started", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "No aircraft attached", Toast.LENGTH_SHORT).show();
        }
    }

}

