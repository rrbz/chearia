# AirPoMS DJI

## A DJI UX SDK based Android application to communicate between DJI drones and AirPoMS sensor and server

Uses libraries from DJI. The rest is distributed with the same license as the AirPoMS package.
