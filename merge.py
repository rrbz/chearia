#!/usr/bin/python3
# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
#
# Usage: [python3] merge.py [--debug] <out.csv> <AIRPOMS.csv>

import sys
import os
from datetime import datetime
import csv

if len(sys.argv) < 2:
    print("No enough arguments")
    print("\nUsage: [python3] merge.py [--debug] <out.csv> <AIRPOMS.csv>")
    exit(1)


for f in sys.argv[-2:]:
    if not os.path.exists(f):
        print(f, "does not exist. Exiting")
        exit(2)


def debug(*msg, **kw):
    if "--debug" in sys.argv:
        print(*msg, **kw)


# Load into memory all the altitudes
alt = open(sys.argv[-2])
alts = {}
for line in csv.reader(alt):
    i = [int(x) for x in line[:6]]
    now = datetime(day=i[0], month=i[1], year=i[2], hour=i[3]+12, minute=i[4], second=i[5])
    debug(now, line)
    alts[now] = line[6:]
alt.close()


# Now read the sensor's file
sns = open(sys.argv[-1])
out = open("merged.csv", "w")
writer = csv.writer(out)
for line in csv.reader(sns):
    debug(line)
    # AirPoMS file has date in cols from 4 to 9
    i = [int(x) for x in line[4:10]]
    now = datetime(day=i[0], month=i[1], year=i[2], hour=i[3], minute=i[4], second=i[5])
    debug(now)
    if now in alts:
        debug(line[3], "(", line[11] , ")", "will be replaced with", alts[now])
        newline = line
        newline[1] = alts[now][0]
        newline[2] = alts[now][1]
        newline[3] = alts[now][2]
        debug("NEW:", newline)
        writer.writerow(newline)

sns.close()
out.close()
