# AirPoMS Sensor Module Embedded Firmware

This is the custom firmware to run the sensor made by FBK written by @j54n1n

## LEDS

### Operational

If all systems are working all leds should be off.
The green led turns on when the measurement start
(every 5 seconds). If some other
led blinks after the green one, see the error section.


### Normal startup

This is what should normally happen when turning
the sensor module on:

* All leds on (red, orange, yellow, green);
* Delay of 0.5s, then the lights turn off one by one every 0.5s in the order red, orange, yellow, green;
* The SD card beings initialized, if the SD card is present, the yellow led will turn on;
* The firmware now tries to open the "AIRPOMS.CSV" file, if it's available the green led will turn on otherwise the red one will turn on;
* The orange led will turn on to warn you BME is being initialized. If BME is reachable and ready the orange led will turn off istantly. If not, please check the hardware.



### Errors

* Orange led blinking 2 times for 0.5s after the green operational led: No signal from the GPS;
