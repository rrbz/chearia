#pragma once

/*
	Name:       GasMapper.h
	Created:	25.01.2019 08:02
	Author:     Julian Sanin
*/

#include <Arduino.h>

namespace gm {

	namespace args {

		enum {
			GPS_BAUD_RATE  = 9600,
			SW_SERIAL_BAUD = 57600,
			SW_SERIAL_RX   = SDA,
			SW_SERIAL_TX   = SCL,
			T_UPDATE_MS    = 4500,
			SD_CS          = 9,
			GAS_CS         = 14,
			BAT_V_IN       = A6,
			LED_RED        = 5,
			LED_ORANGE     = 6,
			LED_YELLOW     = 7,
			LED_GREEN      = 8,
			LED_ON         = HIGH,
			LED_OFF        = LOW,
		};

		const char * const SD_CSV_FILENAME PROGMEM = "AIRPOMS.CSV";
	}

	namespace argv {

		struct Gps {
			uint8_t month;
			uint8_t day;
			uint16_t year;
			uint8_t hour;
			uint8_t minute;
			uint8_t second;
			double latitude;
			double longitude;
			double altitude;
			uint32_t chars;
		};

		struct Bat {
			float V_In;
		};

		struct Gas {
			float temperature;
			float humidity;
			float pressure;
			float resistance;
			float iaq;
			float altitude;
			float zeropress;
		};
	}

	void init();

	void run();
}
