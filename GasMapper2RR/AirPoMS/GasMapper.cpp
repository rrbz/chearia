/*
	Name:       GasMapper.cpp
	Created:	25.01.2019 08:19
	Author:     Julian Sanin
*/
// BETA FIRMWARE WARNING
// THIS CODE HAS BEEN EXTENDED BY MARCO MARINELLO
// USE IT WITH CAUTION: IT MAY KILL UNICORNS (OR BOARDS)

#include "GasMapper.h"
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <SdFat.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME680.h>

using namespace gm;

enum {
	PRINT_SW_SERIAL = (1 << 0),
	PRINT_SD_CARD   = (1 << 1),
};

static void update(TinyGPSPlus & gps, argv::Gps & vGps);
static void update(Adafruit_BME680 & bme, argv::Gas & vGas);
static void update(argv::Bat & vBat);

static void echo(int printOpts, size_t idEntry,
	             argv::Gps & vGps, argv::Gas & vGas, argv::Bat & vBat);
static void echo(Stream & stream, size_t idEntry,
	             argv::Gps & vGps, argv::Gas & vGas, argv::Bat & vBat);

static float IAQ(float current_humidity, float gas_reference);

static SdFat sd;
static SoftwareSerial swSerial(args::SW_SERIAL_RX, args::SW_SERIAL_TX);
static Adafruit_BME680 bme(args::GAS_CS);

float medpress = 0;  // where the first 10 measurements are stored
float zeropress = 0;  // where the pressure at the ground is stored

void gm::init() {
	using namespace args;
	Serial.begin(GPS_BAUD_RATE);
	swSerial.begin(SW_SERIAL_BAUD);
	for (int pin = LED_RED; pin <= LED_GREEN; pin++) {
		pinMode(pin, OUTPUT);
		digitalWrite(pin, LED_ON);
	}
	delay(500);
	digitalWrite(LED_RED,    LED_OFF); delay(500);
	digitalWrite(LED_ORANGE, LED_OFF); delay(500);
	digitalWrite(LED_YELLOW, LED_OFF); delay(500);
	digitalWrite(LED_GREEN,  LED_OFF); delay(500);
	analogReference(INTERNAL); //1.1V reference
	// Init SD card, yellow LED if all OK.
	if (sd.begin(args::SD_CS, SPI_FULL_SPEED)) {
		digitalWrite(LED_YELLOW, LED_ON);
	}
	delay(500);
	// Touch file for logging green led if all ok, red if error.
	File csvFile = sd.open(SD_CSV_FILENAME, FILE_WRITE);
	if (csvFile) {
		digitalWrite(LED_GREEN, LED_ON);
		csvFile.close(); // close the file
	} else {
		digitalWrite(LED_RED, LED_ON);
	}
	delay(500);
	digitalWrite(LED_ORANGE, LED_ON);
	delay(500);
	if (bme.begin()) {
		digitalWrite(LED_ORANGE, LED_OFF);
		bme.setTemperatureOversampling(BME680_OS_8X);
		bme.setHumidityOversampling(BME680_OS_2X);
		bme.setPressureOversampling(BME680_OS_4X);
		bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
		bme.setGasHeater(320, 150); // 320*C for 150 ms
	}
	delay(500);
}

void gm::run() {
	using namespace args;
	static TinyGPSPlus gps;
	static size_t idEntry = 0;
	static uint32_t pastMillis = millis();

	digitalWrite(LED_GREEN, LED_OFF);
	while (Serial.available()) {
		char character = Serial.read();
		if (gps.encode(character)) {
			// Parsed GPS data successfully
		}
	}
	if ((millis() - pastMillis) >= T_UPDATE_MS) {
		pastMillis = millis();
		digitalWrite(LED_GREEN, LED_ON);
		// Push updated data
		argv::Gps vGps;
		argv::Gas vGas;
		argv::Bat vBat;
		vGas.zeropress = zeropress;
		update(bme, vGas);
		update(gps, vGps);
		update(vBat);
		echo(PRINT_SW_SERIAL | PRINT_SD_CARD, idEntry, vGps, vGas, vBat);
		if (idEntry <= 9) {
			medpress += vGas.pressure;
		}
		if (idEntry == 9) {
			zeropress = medpress / 10;
			digitalWrite(LED_GREEN, LED_OFF);
			digitalWrite(LED_YELLOW, LED_ON);
			delay(500);
			digitalWrite(LED_YELLOW, LED_OFF);
			delay(500);
			digitalWrite(LED_YELLOW, LED_ON);
			delay(500);
			digitalWrite(LED_YELLOW, LED_OFF);
		}
		if (vGps.latitude == 0.0 || vGps.longitude == 0.0) {
			// blink two times to warn the user gps is till not working
			// shut down the green led first to make sure it is visible
			digitalWrite(LED_GREEN, LED_OFF);
			digitalWrite(LED_ORANGE, LED_ON);
			delay(500);
			digitalWrite(LED_ORANGE, LED_OFF);
			delay(500);
			digitalWrite(LED_ORANGE, LED_ON);
			delay(500);
			digitalWrite(LED_ORANGE, LED_OFF);
		}
		idEntry += 1;
	}
}

void update(TinyGPSPlus & gps, argv::Gps & vGps) {
	vGps.month = gps.date.month();
	vGps.day = gps.date.day();
	vGps.year = gps.date.year();
	vGps.hour = gps.time.hour();
	vGps.minute = gps.time.minute();
	vGps.second = gps.time.second();
	vGps.latitude = gps.location.lat();
	vGps.longitude = gps.location.lng();
	vGps.altitude = gps.altitude.meters();
	vGps.chars = gps.charsProcessed();
}

void update(Adafruit_BME680 & bme, argv::Gas & vGas) {
	bme.performReading();
	vGas.temperature = bme.temperature;
	vGas.humidity = bme.humidity;
	vGas.pressure = bme.pressure / 100;
	vGas.resistance = bme.gas_resistance / 1000;
	vGas.iaq = IAQ(vGas.humidity, vGas.resistance);
	vGas.altitude = -1;
	if (vGas.zeropress > 0) {
		vGas.altitude = 44330.0 * (1.0 - pow(vGas.pressure / vGas.zeropress, 0.1903));
	}
}

void update(argv::Bat & vBat) {
	vBat.V_In = analogRead(args::BAT_V_IN)*0.02256; // V/1.024* 1.1 * 21 actual value is divided by 3
}

void echo(int printOpts, size_t idEntry,
	      argv::Gps & vGps, argv::Gas & vGas, argv::Bat & vBat) {
	if (printOpts & PRINT_SW_SERIAL) {
		echo(swSerial, idEntry, vGps, vGas, vBat);
	}
	if (printOpts & PRINT_SD_CARD) {
		File csvFile = sd.open(gm::args::SD_CSV_FILENAME, FILE_WRITE);
		if (csvFile) {
			echo(csvFile, idEntry, vGps, vGas, vBat);
		}
		csvFile.close();
		// Put card in power save mode.
		sd.card()->spiStop();
	}
}

void echo(Stream & stream, size_t idEntry,
	      argv::Gps & vGps, argv::Gas & vGas, argv::Bat & vBat) {
	digitalWrite(gm::args::LED_YELLOW, gm::args::LED_ON);
	// ID:
	stream.print(idEntry);          stream.print(',');
	// GPS stats:
	stream.print(vGps.latitude, 6);  stream.print(',');
	stream.print(vGps.longitude, 6); stream.print(',');
	stream.print(vGps.altitude);     stream.print(',');
	stream.print(vGps.day);          stream.print(',');
	stream.print(vGps.month);        stream.print(',');
	stream.print(vGps.year);         stream.print(',');
	stream.print(vGps.hour);         stream.print(',');
	stream.print(vGps.minute);       stream.print(',');
	stream.print(vGps.second);       stream.print(',');
	// Gas stats:
	stream.print(vGas.temperature);  stream.print(',');
	stream.print(vGas.humidity);     stream.print(',');
	stream.print(vGas.pressure);     stream.print(',');
	stream.print(vGas.resistance);   stream.print(',');
	stream.print(vGas.iaq);          stream.print(',');
	// Extra stats:
	stream.print(vBat.V_In);         stream.print(',');
	stream.print(vGas.altitude);     stream.print(',');
	stream.print(vGas.zeropress);    stream.print(',');
	stream.print(vGps.chars);        stream.print('\n');
	digitalWrite(gm::args::LED_YELLOW, gm::args::LED_OFF);
}

float IAQ(float current_humidity, float gas_reference) {
	float hum_weighting = 0.25; // so hum effect is 25% of the total air quality score
	float gas_weighting = 0.75; // so gas effect is 75% of the total air quality score

	float hum_score, gas_score;
	float hum_reference = 40;


	if (current_humidity >= 38 && current_humidity <= 42)
		hum_score = 0.25 * 100; // Humidity +/-5% around optimum
	else
	{ //sub-optimal
		if (current_humidity < 38)
			hum_score = 0.25 / hum_reference * current_humidity * 100;
		else
			hum_score = ((-0.25 / (100 - hum_reference)*current_humidity) + 0.416666) * 100;
	}

	//Calculate gas contribution to IAQ index
	int gas_lower_limit = 5;   // Bad air quality limit
	int gas_upper_limit = 50;  // Good air quality limit
	if (gas_reference > gas_upper_limit) gas_reference = gas_upper_limit;
	if (gas_reference < gas_lower_limit) gas_reference = gas_lower_limit;
	gas_score = (0.75 / (gas_upper_limit - gas_lower_limit)*gas_reference - (gas_lower_limit*(0.75 / (gas_upper_limit - gas_lower_limit)))) * 100;

	//Combine results for the final IAQ index value (0-100% where 100% is good quality air)
	return  (500 - 5 * (hum_score + gas_score));
}
