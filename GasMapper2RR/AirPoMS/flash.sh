#!/bin/bash

echo -n "Are you sure the .hex file is compiled for 3.3V 8Mhz? "
read sure

if ! [ "yes" = $sure ] ; then exit 1; fi

echo -n "Are you REALLY sure the .hex file is compiled for 3.3V 8Mhz? "
read sure

if ! [ "yes" = $sure ] ; then exit 1; fi

echo "Now flashing"

AVRDUDE="/usr/bin/avrdude"

if [ ! -e $AVRDUDE ] ; then AVRDUDE="/opt/arduino-1.8.8/hardware/tools/avr/bin/avrdude"; fi

$AVRDUDE -v -patmega328p -cusbtiny -U./AirPoMS.ino.eightanaloginputs.hex -Ulock:w:0x3F:m -Uefuse:w:0xFD:m -Uhfuse:w:0xDA:m -Ulfuse:w:0xFF:m
