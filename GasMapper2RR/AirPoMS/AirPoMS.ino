/*
    Name:       AirPoMS.ino
    Created:	24.01.2019 22:08
    Author:     Julian Sanin
*/

#include "GasMapper.h"

void setup() {
	gm::init();
}

void loop() {
	gm::run();
}
