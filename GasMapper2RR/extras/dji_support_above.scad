cube([30, 30, 2]);

difference() {
    translate([15, 15, 0]) cylinder(20, d=15);
    translate([5, 13.5, 16]) cube([50, 3, 10]);
}
