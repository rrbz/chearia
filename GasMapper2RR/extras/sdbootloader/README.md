# Sources of SD card bootloader (pfboot)

This folder contains the bootloader used within the sensor module for the project AirPoMS.
The bootloader has been modified to support the *ATmega328p MCU* with *F_CPU 8MHz*.

## Build environment

The project can be build in *Release* mode on Windows with Atmel Studio v6.1 SP1, otherwise use some
Linux OS with packages build-essential gcc-avr, avr-libc installed and execute `make all` from the
*src/Build* directory. Optionally install also avrdude to flash the bootloader.

Any changes within the *pfboot* directory must be reported to the *src* folder by invoking
`./studio6.1_linux_conversion.sh pfboot` script.


## Flash bootloader
On Windows you can use the tools integrated with Arduino IDE and the example sketch *ArduinoISP*.
On Linux use *avrdude* and adapt follwing commandline:
```
avrdude -C/etc/avrdude.conf -v -patmega328p -cstk500v1 -PCOM4 -b19200
avrdude -C/etc/avrdude.conf -v -patmega328p -cstk500v1 -PCOM4 -b19200 -e -Ulock:w:0x3F:m -Uefuse:w:0xFD:m -Uhfuse:w:0xDA:m -Ulfuse:w:0xFF:m
avrdude -C/etc/avrdude.conf -v -patmega328p -cstk500v1 -PCOM4 -b19200 -D -Uflash:w:pfboot-atmega328p-8MHz-AirPoMS.hex:i
```

Or you can use also an ATtiny device such as the *Olimexino-85s* with *Little Wire* as USBtinyISP:
```
avrdude -C/etc/avrdude.conf -v -patmega328p -cusbtiny -U pfboot-atmega328p-8MHz-AirPoMS.hex -Ulock:w:0x3F:m -Uefuse:w:0xFD:m -Uhfuse:w:0xDA:m -Ulfuse:w:0xFF:m
```

See also:
 * https://www.olimex.com/Products/Duino/AVR/OLIMEXINO-85S/open-source-hardware
 * https://digistump.com/wiki/digispark/tutorials/programming#using_littlewire_as_usbtinyisp
 * http://littlewire.github.io/


## Sources

All the source codes for this bootloader where obtained from following sites and are achived in the
*sources* folder:
 * https://spaces.microchip.com/gf/project/sdbootloader/
 * https://www.avrfreaks.net/comment/822148#comment-822148
