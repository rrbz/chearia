#!/bin/sh

PROJECT_NAME=$1

if [ "$PROJECT_NAME" != "" ]; then

  TARGET_DIR=${PROJECT_NAME}/Release

  find ${TARGET_DIR} -iname "*.d" -type f -exec sed -i 's/c:\\program\\ files\\ (x86)\\atmel\\atmel\\ toolchain\\avr8\\ gcc\\native\\3.4.2.1002\\avr8-gnu-toolchain\\bin\\//g' {} \;
  sed -i 's/cmd.exe/sh/g' ${TARGET_DIR}/Makefile
  sed -i 's/ : 3.4.2//g' ${TARGET_DIR}/Makefile
  sed -i 's/(AVR_8_bit_GNU_Toolchain_3.4.0_663)/AVR_8_bit_GNU_Toolchain_3.4.0_663/g' ${TARGET_DIR}/Makefile
  sed -i 's/C:\\Program Files (x86)\\Atmel\\Atmel Toolchain\\AVR8 GCC\\Native\\3.4.2.1002\\avr8-gnu-toolchain\\bin\\avr-gcc.exe/avr-gcc/g' ${TARGET_DIR}/Makefile
  sed -i 's/C:\\Program Files (x86)\\Atmel\\Atmel Toolchain\\AVR8 GCC\\Native\\3.4.2.1002\\avr8-gnu-toolchain\\bin\\avr-objcopy.exe/avr-objcopy/g' ${TARGET_DIR}/Makefile
  sed -i 's/C:\\Program Files (x86)\\Atmel\\Atmel Toolchain\\AVR8 GCC\\Native\\3.4.2.1002\\avr8-gnu-toolchain\\bin\\avr-size.exe/avr-size/g' ${TARGET_DIR}/Makefile
  sed -i 's/C:\\Program Files (x86)\\Atmel\\Atmel Toolchain\\AVR8 GCC\\Native\\3.4.2.1002\\avr8-gnu-toolchain\\bin\\avr-objdump.exe/avr-objdump/g' ${TARGET_DIR}/Makefile

  mkdir -p src
  cp -r ${TARGET_DIR}/../* src/
  cp -r src/Release src/Build

else

  echo "Usage: studio6_linux_conversion.sh PROJECT_NAME (e.g. the project directory name)"

fi
