// check if is preview or rendering
// WARN: This is only supported by the latest
// versions of OpenSCAD
// see https://www.thingiverse.com/groups/openscad/forums/general/topic:26267
$preview = $preview==undef? F5: $preview;

// neck
translate([0, 0, 0])
    cube([40, 2, 10]);
// main cube
translate([0, 0, 10])
    cube([40, 30, 1]);
// battery holder
translate([0, 21, 4])
    cube([40, 3, 7]);
// bottom
translate([0, 0, -1]) cube([40, 30, 1]);
// holder
translate([0, 14, -4]) cube([40, 2, 4]);


if ($preview) {
// battery
#translate([5, 2, 3]) cube([25, 18, 6]);

// sensor
#translate([47, 0, 13]) rotate([0, 0, 90])
    import("stl/gasmapper.stl");
}