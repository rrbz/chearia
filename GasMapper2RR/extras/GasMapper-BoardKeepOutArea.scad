// Keep out areas of the GasModuleV2.
// All units in mm.
// Origin is left bottom.

BOARD_THICKNESS = 1.8;
BOARD_OUTER_WIDTH = 32.2;
BOARD_INNER_WIDTH = 26;
BOARD_TOP_LENGTH = 15;
BOARD_TOTAL_LENGTH = 48.6;

DRILL_DIAMETER = 3;
DRILL_HOLE_0 = [3.6, 3.6]; // [0]->x, [1]->y
DRILL_HOLE_1 = [BOARD_OUTER_WIDTH-3.6, 3.6];
DRILL_HOLE_2 = [BOARD_OUTER_WIDTH-3.6, 3.6+25];
DRILL_HOLE_3 = [3.6, 3.6+25];
$fn = 16;

GPS_HEIGHT = 7;
GPS_SIDE = 17;

SD_HEIGHT = 2;
SD_WIDTH = 14;
SD_LENGTH = 18;
SD_POS = [8, -5];

USB_HEIGTH = 3;
USB_WIDTH = 8;
USB_LENGTH = 6;
USB_POS = [12, 0];

ISP_HEIGTH = 12;
ISP_WIDTH = 5;
ISP_LENGTH = 8;
ISP_POS = [26, 16];

I2C_HEIGTH = 12;
I2C_WIDTH = 2.5;
I2C_LENGTH = 10;
I2C_POS = [0.8, 15.3];

module PCB() {
    cube([
        BOARD_OUTER_WIDTH,
        BOARD_TOTAL_LENGTH-BOARD_TOP_LENGTH,
        BOARD_THICKNESS]
    );
    translate([
        BOARD_OUTER_WIDTH/2-BOARD_INNER_WIDTH/2,
        BOARD_TOTAL_LENGTH-BOARD_TOP_LENGTH,
        0]
    ) /* GPS board area */ {
        cube([
            BOARD_INNER_WIDTH,
            BOARD_TOP_LENGTH,
            BOARD_THICKNESS]
        );
    }
}

module DrillHoles() {
    translate([DRILL_HOLE_0[0],DRILL_HOLE_0[1],0]) {
        cylinder(h=BOARD_THICKNESS,d=DRILL_DIAMETER);
    }
    translate([DRILL_HOLE_1[0],DRILL_HOLE_1[1],0]) {
        cylinder(h=BOARD_THICKNESS,d=DRILL_DIAMETER);
    }
    translate([DRILL_HOLE_2[0],DRILL_HOLE_2[1],0]) {
        cylinder(h=BOARD_THICKNESS,d=DRILL_DIAMETER);
    }
    translate([DRILL_HOLE_3[0],DRILL_HOLE_3[1],0]) {
        cylinder(h=BOARD_THICKNESS,d=DRILL_DIAMETER);
    }
}

module GPS() {
    translate([
        BOARD_OUTER_WIDTH/2-GPS_SIDE/2,
        BOARD_TOTAL_LENGTH-GPS_SIDE,
        BOARD_THICKNESS]
    ) {
        cube([GPS_SIDE, GPS_SIDE, GPS_HEIGHT]);
    }
}

module SD() {
    translate([
        SD_POS[0],
        SD_POS[1],
        BOARD_THICKNESS]
    ) {
        cube([SD_WIDTH, SD_LENGTH, SD_HEIGHT]);
    }
}

module MicroUSB() {
    translate([
        USB_POS[0],
        USB_POS[1],
        -USB_HEIGTH
    ]) {
        cube([USB_WIDTH, USB_LENGTH, USB_HEIGTH]);
    }
}

module ISP() {
    translate([
        ISP_POS[0],
        ISP_POS[1],
        -BOARD_THICKNESS
    ]) {
        cube([ISP_WIDTH, ISP_LENGTH, ISP_HEIGTH]);
    }
}

module I2C() {
    translate([
        I2C_POS[0],
        I2C_POS[1],
        -BOARD_THICKNESS
    ]) {
        cube([I2C_WIDTH, I2C_LENGTH, I2C_HEIGTH]);
    }
}

difference() {
    union() {
        PCB();
        GPS();
        SD();
        MicroUSB();
        ISP();
        I2C();
    }
    DrillHoles();
}