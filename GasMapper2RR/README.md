# Sources of Che Aria Sensor Module

A project in collaboration with FBK Junior Trento and Rainerum Robotics.

### Usage Instructions

*Clone this repository* with git command line or Git for Windows or else you can
also *download the repository as a ZIP file*. Then put the project folder
somewhere on your filesystem of your PC. Launch your Arduino installation and go
to the menu bar `File | Preferences`. In the `Settings` tab click the `Browse`
button on the right side of `Sketchbook location:` and select the *project
folder that you have just copied*. Hit the `OK` button and close the Arduino IDE
without saving the empty sketch. Now after every restart of the IDE you can
launch any sketch that resides within the project folder and any included
software library within the library folder will be available for your sketches.

### Environment

The sketches have been developed with the *Arduino IDE 1.8.7* from the
[arduino.cc](https://www.arduino.cc/) website. You can expect that the sketches
can be executed for any future release of the IDE. Any previous release of the
IDE is not guaranteed to work as it could be that specific properties or
behaviours of the IDE are expected or used.

As a second option for developing software for Arduino you can use the *Visual
Studio* plugin [Visual Micro](http://www.visualmicro.com/). The Visual Studio
IDE can nowadays be downloaded from [Mircosoft](https://www.visualstudio.com/)
for free as a *Community* edition, that needs only a valid registered Microsoft
account such that it works properly.

### Board Layout

The board has several components that are extrudign from the PCB. A CAD file has
been included in the *extras* folder to facilitate external case designs.

![Board Keep Out Area](extras/GasMapper-BoardKeepOutArea.png)

The In System Programming (ISP) port can be found on the right side of the board.
**Pay attention the board has 3.3V IO voltage levels and power supply.**

![Board Gerber Layers](extras/board-layers.png)

On the left side there is also available the I2C interface. Between GPS module
and microcontroller there is the shared port from the Arduino hardware serial.

### Pinout

The Quectel L80 GPS connected directly to the hardware serial of the ATmega328P.
 * D0 GPS_RX
 * D1 GPS_TX
 * D2 GPS_RST

![Quectel L80 GPS Pinout](extras/L80-GPS-pinout.png)

Available solder pads:
```
COM ,-----.
    | o o |
    `-----´
     TX RX
```

The BME680 environmental sensor is connected to the SPI bus that is shared also
with the SD card interface and the In System Programming (ISP) connector of the
microcontroller.
 * A0 (D14) GAS_CS
 * D13 SPI_SCK
 * D12 SPI_MISO
 * D11 SPI_MOSI
 * D9 SD_CS

![BOSCH BME680 Pinout](extras/BME680-sensor-pinout.png)

Available solder pads:
```
        ISP
     ,-----.
GND  | o o | RST
MOSI | o o | SCK
3.3V | o o | MISO
     `-----´
```

Further information can be found in the *extras* folder.

#### Further Pin Mappings

The board has an unused hardware I2C port available.
 * A4 (D18) I2C_SDA
 * A5 (D19) I2C_SCL
 
A software serial can be applied to this pins (*).
 
Available solder pads:
```
,---.
| o | SDA (SW RXD)
| o | SCL (SW TXD)
| o | GND
| o | 3.3V
`---´
  I2C
```

The main power supply voltage can be measured against the internal 1.1V
voltage reference of the microcontroller (for example from the USB port).
 * A6 (D20) V_IN

The onboard LEDs are driven from following pins:
 * D5 LED1 Red
 * D6 LED2 Orange
 * D7 LED3 Yellow
 * D8 LED4 Green
 
All pinout descriptions follow the *Arduino Pro / ATmega328P TQFP* board
definiton.

![Arduino Pro Pinout](extras/atmega328pb_rev1_pinout.png)

### Platform

The sketches have been developed for the *Arduino Pro 3.3V 8 MHz*. Any
other board with an *ATmega328* compatible microcontroller might also work but
our mileage may vary.

### Useful Links
* https://www.arduino.cc/
* https://www.visualstudio.com/
* http://www.visualmicro.com/