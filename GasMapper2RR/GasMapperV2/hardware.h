#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"
#include <TinyGPS.h>

//======== PINS ======================
int LED1   = 5;        // RED LED pin, ~0.1 mA
int LED2   = 6;        // ORG LED pin, ~0.1 mA
int LED3   = 7;        // YLW LED pin, ~0.1 mA
int LED4   = 8;        // GRN LED pin, ~0.1 mA

int GPS_RST = 2;      // GPS reset pin

int SD_CS   = 9;      // SD card cs pin

int GAS_CS  = 14;     // gas sensor cs pin 
int LORA_RST = 4;     // LORA reset pin
int LORA_nSS = 3;     // LORA nSS pin
int LORA_DIO0 = 17;   // LORA DIO_0 pin
int LORA_DIO1 = 16;   // LORA DIO_1 pin
int LORA_DIO2 = 15;   // LORA DIO_2 pin

int V_in  = A6;      // voltage divider from Vin


float IAQ(float current_humidity, float gas_reference){
  float hum_weighting = 0.25; // so hum effect is 25% of the total air quality score
  float gas_weighting = 0.75; // so gas effect is 75% of the total air quality score
  
  float hum_score, gas_score;
  float hum_reference = 40;


  if (current_humidity >= 38 && current_humidity <= 42)
    hum_score = 0.25*100; // Humidity +/-5% around optimum 
  else
  { //sub-optimal
    if (current_humidity < 38) 
      hum_score = 0.25/hum_reference*current_humidity*100;
    else
      hum_score = ((-0.25/(100-hum_reference)*current_humidity)+0.416666)*100;
  }
  
  //Calculate gas contribution to IAQ index
  int gas_lower_limit = 5;   // Bad air quality limit
  int gas_upper_limit = 50;  // Good air quality limit 
  if (gas_reference > gas_upper_limit) gas_reference = gas_upper_limit; 
  if (gas_reference < gas_lower_limit) gas_reference = gas_lower_limit;
  gas_score = (0.75/(gas_upper_limit-gas_lower_limit)*gas_reference -(gas_lower_limit*(0.75/(gas_upper_limit-gas_lower_limit))))*100;
  
  //Combine results for the final IAQ index value (0-100% where 100% is good quality air)
  return  ( 500 - 5*(hum_score + gas_score));
}
