#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME680.h>
#include <Wire.h>
#include <TinyGPS.h>

#include "hardware.h"

TinyGPS gps;
Adafruit_BME680 bme(GAS_CS); // hardware SPI
File sdcard_file;

unsigned int id;
static void smartdelay(unsigned long ms);

void setup() {
//  pinMode(SD_CS,OUTPUT);
//  pinMode(GAS_CS,OUTPUT);
  
  pinMode(LED1,OUTPUT);
  pinMode(LED2,OUTPUT);
  pinMode(LED3,OUTPUT);
  pinMode(LED4,OUTPUT);

  Serial.begin(9600);   // GPS serial

//  digitalWrite(SD_CS, HIGH);
//  digitalWrite(GAS_CS, HIGH);
  
  digitalWrite(LED1, HIGH);
  digitalWrite(LED2, HIGH);
  digitalWrite(LED3, HIGH);
  digitalWrite(LED4, HIGH);
  delay(500);
  digitalWrite(LED1, LOW);
  delay(500);
  digitalWrite(LED2, LOW);
  delay(500);
  digitalWrite(LED3, LOW);
  delay(500);
  digitalWrite(LED4, LOW);
  delay(500);
 
  analogReference(INTERNAL); //1.1V reference
  
  
// =================================  
// Init SDcard, yellow LED if all OK
  if (SD.begin(SD_CS))
    digitalWrite(LED3, HIGH);
  delay(500);

// =================================
//  create / verify file for logging
// green led if all ok, red if error
  sdcard_file = SD.open("test.txt", FILE_WRITE);
  if (sdcard_file){ 
    digitalWrite(LED4, HIGH); //GREEEN
    sdcard_file.close(); // close the file
  } else 
    digitalWrite(LED1, HIGH); //red
  
  delay(500);
  
// =================================  
// Init BME680, yellow LED if all OK

  if (bme.begin()){     //connect to gas sensor
    digitalWrite(LED2, HIGH); //GREEEN
    bme.setTemperatureOversampling(BME680_OS_8X);
    bme.setHumidityOversampling(BME680_OS_2X);
    bme.setPressureOversampling(BME680_OS_4X);
    bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
    bme.setGasHeater(320, 150); // 320*C for 150 ms 
  }
  delay(500);  

  id = 0;
}

void loop() {
  int val = 0;
  long latitude = 0, longitude = 0;
  float Q = 0, Vbat = 0, T= 0, P=0, H=0, R=0;
  unsigned long age = 0, chars = 0;
  unsigned short sentences = 0, failed = 0;

  int year = 0;
  byte month = 0, day = 0, hour = 0, minute = 0, second = 0, hundredths = 0;
  while (1){
    digitalWrite(LED4, LOW);
    smartdelay(4500);
    digitalWrite(LED4, HIGH);
    
    bme.performReading();
    T = bme.temperature;
    P = bme.pressure / 100;
    H = bme.humidity;
    R = bme.gas_resistance / 1000;
    Q = IAQ(H, R);
    delay(100);
    
    gps.get_position(&latitude, &longitude, &age);
    gps.stats(&chars, &sentences, &failed);
    gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
    
    Vbat = analogRead(A6)*0.02256;             // V/1.024* 1.1 * 21 actual value is divided by 3
   
    
    
    
    sdcard_file = SD.open("test.txt", FILE_WRITE);
    if (sdcard_file){
      digitalWrite(LED3, HIGH);
      sdcard_file.print(id);          sdcard_file.print("\t");
      sdcard_file.print(month);       sdcard_file.print("/");
      sdcard_file.print(day);         sdcard_file.print("/");
      sdcard_file.print(year);        sdcard_file.print("\t");
      
      sdcard_file.print(hour);        sdcard_file.print(":");
      sdcard_file.print(minute);      sdcard_file.print(":");
      sdcard_file.print(second);      sdcard_file.print("\t");
      
      sdcard_file.print(latitude);    sdcard_file.print("\t");
      sdcard_file.print(longitude);   sdcard_file.print("\t");

      sdcard_file.print(T);           sdcard_file.print("\t");
      sdcard_file.print(H);           sdcard_file.print("\t");
      sdcard_file.print(P);           sdcard_file.print("\t");
      sdcard_file.print(R);           sdcard_file.print("\t");
      sdcard_file.print(Q);           sdcard_file.print("\t");

      sdcard_file.print(Vbat);        sdcard_file.print("\t");
      sdcard_file.print(chars);       sdcard_file.print("\n");
      sdcard_file.close();
    }
    else
      digitalWrite(LED3, LOW);

    id++;
  }
}



static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (Serial.available())
      gps.encode(Serial.read());
  } while (millis() - start < ms);
}


