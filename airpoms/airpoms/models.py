# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.db.models import Max, Avg
from django.utils.translation import ugettext_lazy as _
from airpoms.utils import iaq_color


class Country(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    code = models.CharField(max_length=200, verbose_name=_("name"), unique=True)

    def __str__(self):
        return "%s (%s)" %(self.name, self.code)


    class Meta:
        verbose_name = _("Country")
        verbose_name_plural = _("Countries")
        ordering = ["name"]


class Region(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    country = models.ForeignKey(Country, verbose_name=_("country"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("Region")
        verbose_name_plural = _("Regions")
        ordering = ["country", "name"]


class Province(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    region = models.ForeignKey(Region, verbose_name=_("region"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("Province")
        verbose_name_plural = _("Provinces")
        ordering = ["region", "name"]


class City(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    province = models.ForeignKey(Province, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")
        ordering = ["province", "name"]


class Property(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    unit = models.CharField(max_length=200, null=True, blank=True, verbose_name=_("unit"))

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("Property")
        verbose_name_plural = _("Properties")
        ordering = ["name"]


class MonitoredArea(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name=_("city"))

    @property
    def latest_iaq(self):
        try:
            return self.measurement_set.order_by(
                "-date"
            )[0].session.measurementsessioncache_set.get(
                property__name="IAQ"
            )
        except (IndexError, AttributeError):
            return None

    def qualitycolor(self):
        if getattr(self, "latest_iaq"):
            return iaq_color(self.latest_iaq.value)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("Monitored area")
        verbose_name_plural = _("Monitored areas")
        ordering = ["name"]


class MeasurementSession(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("name"))
    area = models.ForeignKey(MonitoredArea, on_delete=models.CASCADE, verbose_name=_("area of monitoring"))
    notes = models.TextField(null=True, blank=True, verbose_name=_("annotations"))

    @property
    def maxaltitude(self):
        try:
            q = MeasurementValue.objects.filter(measurement__session=self)
            return q.filter(property__name="Altitude").aggregate(Max("value"))["value__max"]
        except Exception as e:
            return 0

    def lat_center(self):
        try:
            qs = MeasurementValue.objects.filter(measurement__session=self, property__name="Latitude")
            avglat = qs.aggregate(Avg("value"))["value__avg"]
        except:
            avglat = 0
        return avglat

    def long_center(self):
        try:
            qs = MeasurementValue.objects.filter(measurement__session=self, property__name="Longitude")
            avglong = qs.aggregate(Avg("value"))["value__avg"]
        except:
            avglong = 0
        return avglong

    @property
    def is_altitude_scanned(self):
        qs = MeasurementValue.objects.filter(measurement__session=self, property__name="Altitude")
        return qs.exists()


    def __str__(self):
        return "%s %s %s %s" % (_("Measurement session"), self.name, _("in"), self.area)


    class Meta:
        verbose_name = _("Measurement session")
        verbose_name_plural = _("Measurement sessions")
        ordering = ["area", "name"]


class Measurement(models.Model):
    area = models.ForeignKey(MonitoredArea, on_delete=models.CASCADE, verbose_name=_("area of monitoring"))
    session = models.ForeignKey(MeasurementSession, on_delete=models.CASCADE, verbose_name=_("measurement session"))
    date = models.DateTimeField(verbose_name=_("date and time of the measurement"))

    def qualitycolor(self):
        q = self.measurementvalue_set.filter(property__name="IAQ")
        if q.exists():
            return q[0].qualitycolor()
        return "#FFFFFF"

    def __str__(self):
        return "%s %s %s %s" % (_("Measurement of"), self.date.strftime("%d/%m/%Y %H:%M"), _("in"), self.area)


    class Meta:
        verbose_name = _("Measurement")
        verbose_name_plural = _("Measurements")
        ordering = ["area", "date"]


class MeasurementValue(models.Model):
    measurement = models.ForeignKey(Measurement, on_delete=models.CASCADE, verbose_name=_("measurement"))
    property = models.ForeignKey(Property, on_delete=models.CASCADE, verbose_name=_("property"))
    value = models.FloatField(verbose_name=_("value"))

    def qualitycolor(self):
        if self.property.name == "IAQ":
            return iaq_color(self.value)

    def __str__(self):
        return "%s %s %s" %(self.property, _("of"), self.measurement)


    class Meta:
        verbose_name = _("Measurement value")
        verbose_name_plural = _("Measurements values")
        ordering = ["measurement", "property"]


# This model is intended to save caches of the measurements
# (for example, the average IRQ)
class MeasurementSessionCache(models.Model):
    session = models.ForeignKey(MeasurementSession, on_delete=models.CASCADE, verbose_name=_("measurement session"))
    property = models.ForeignKey(Property, on_delete=models.CASCADE, verbose_name=_("property"))
    value = models.FloatField(verbose_name=_("value"))

    def __str__(self):
        return "%s %s %s" %(self.property, _("of"), self.session)


    class Meta:
        verbose_name = _("Measurement value cache")
        verbose_name_plural = _("Measurements values caches")
        ordering = ["session", "property"]
