# Force Django to translate extra strings

from django.utils.translation import ugettext_lazy as _

properties = [
    _("Latitude"),
    _("Longitude"),
    _("Altitude"),
    _("Temperature"),
    _("Humidity"),
    _("Pressure"),
    _("GasSensorResistance"),
    _("IAQ"),
    _("BatteryVoltage")
]
