# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin
from airpoms.models import *


def prepareAdmin(model, search_fields=None, list_display=None):
    class MyModelAdmin(admin.ModelAdmin):
        pass
    if search_fields:
        MyModelAdmin.search_fields = search_fields
    if list_display:
        MyModelAdmin.list_display = list_display
    admin.site.register(model, MyModelAdmin)


prepareAdmin(Country, ["name"])
prepareAdmin(Region, ["name"], ["name", "country"])
prepareAdmin(Province, ["name"], ["name", "region"])
prepareAdmin(City, ["name"], ["name", "province"])
prepareAdmin(Property, ["name"])
prepareAdmin(MonitoredArea, ["name"])
prepareAdmin(MeasurementSession, ["area__name", "name"], ["area", "name"])
prepareAdmin(Measurement, ["date", "area__name"], ["area", "date"])
prepareAdmin(MeasurementValue, ["property__name", "measurement__area__name"], ["measurement", "property", "value"])
prepareAdmin(MeasurementSessionCache, ["session__name", "session__area__name"], ["session", "property"])
