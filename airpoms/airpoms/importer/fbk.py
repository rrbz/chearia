# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
from datetime import datetime
from . import AirPoMSCSVImporter, IgnoredColumn


class FBKCSVImporter(AirPoMSCSVImporter):
    standard_model = [
        IgnoredColumn(), # number
        IgnoredColumn(), # date
        IgnoredColumn(), # time
        "Latitude",
        "Longitude",
        "Temperature",
        "Humidity",
        "Pressure",
        "GasSensorResistance",
        "IAQ",
        "BatteryVoltage",
    ]

    standard_timecols = {}


    def get_reader(self):
        for line in self.raw:
            print(line)
            yield line.split("\t")


    def datetime_parser(self, values):
        print(values)
        try:
            as_str = values[1] + " " + values[2]
        except:
            return None
        try:
            date = datetime.strptime(as_str, "%m/%d/%Y %H:%M:%S")
        except Exception as e:
            print("No datetime", e)
            return None
        return date


    def prepare_line(self, line):
        line = [a.strip() for a in line]
        line[3] = "%s.%s" % (line[3][:2], line[3][2:])
        line[4] = "%s.%s" % (line[4][:2], line[4][2:])
        return line
