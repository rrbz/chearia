# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
from datetime import datetime
from airpoms import models
from django.db.models import Avg
import json


def generate_session_caches(session):
    models.MeasurementSessionCache.objects.filter(session=session).delete()
    values = models.MeasurementValue.objects.filter(measurement__session=session)
    if len(values) == 0:
        raise Exception("No values to be cached.")
    for prop in models.Property.objects.all():
        qs = values.filter(property=prop)
        if len(qs) == 0:
            continue
        models.MeasurementSessionCache.objects.create(
            session=session,
            property=prop,
            value=list(qs.aggregate(Avg("value")).values())[0]
        )
