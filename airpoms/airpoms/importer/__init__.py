# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
from datetime import datetime
from airpoms import models
from django.db.models import Avg
import json
from airpoms.importer.cache import generate_session_caches


class IgnoredColumn:
    pass


class AirPoMSCSVImporter:
    standard_model = [
        IgnoredColumn(), # number
        "Latitude",
        "Longitude",
        "Altitude",
        IgnoredColumn(), # day
        IgnoredColumn(), # month
        IgnoredColumn(), # year
        IgnoredColumn(), # hour
        IgnoredColumn(), # minutes
        IgnoredColumn(), # seconds
        "Temperature",
        "Humidity",
        "Pressure",
        "GasSensorResistance",
        "IAQ",
        "BatteryVoltage",
    ]

    standard_timecols = {
        "day": 4,
        "month": 5,
        "year": 6,
        "hour": 7,
        "minute": 8,
        "second": 9
    }

    def __init__(self, file=None, content=None, csvmodel=None, timecols=None, area=None, session=None):
        if not session:
            raise Exception("Missing importing session")
        if type(session) == str:
            session = models.MeasurementSession.objects.get(name=session)
        self.session = session
        self.area = session.area
        if not csvmodel:
            csvmodel = self.standard_model
        self.compile_model(csvmodel)
        if not timecols:
            timecols = self.standard_timecols
        self.timecols = timecols
        if content:
            if type(content) == str:
                content = content.split("\n")
            self.raw = content
            return
        hd = open(file)
        self.raw = hd.read().split("\n")
        hd.close()


    def compile_model(self, model):
        self.csvmodel = []
        for i in model:
            if type(i) == IgnoredColumn:
                self.csvmodel.append(i)
                continue
            self.csvmodel.append(models.Property.objects.get(name=i))


    def get_reader(self):
        return csv.reader(self.raw)


    def run(self):
        for line in self.get_reader():
            if "_IMPORTER_IGNORE_LINE_" not in line:
                self.do_import(line)


    def as_generator(self, use_json=False):
        i = 0
        status = {"total": len(self.raw), "status": "Importing...", "do_redirect": False}
        for line in self.get_reader():
            if "_IMPORTER_IGNORE_LINE_" not in line:
                self.do_import(line)
                i += 1
                status["current"] = i
                status["perc"] = i/float(status["total"])*100
                if use_json:
                    yield json.dumps(status) + "\n"
                else:
                    yield status
        status["status"] = "Generating caches..."
        if use_json:
            yield json.dumps(status) + "\n"
        else:
            yield status
        generate_session_caches(self.session)
        status["status"] = "Import complete"
        status["do_redirect"] = True
        if use_json:
            yield json.dumps(status) + "\n"
        else:
            yield status


    def datetime_parser(self, values):
        try:
            dt = {x: int(values[self.timecols[x]]) for x in self.timecols}
        except (IndexError, ValueError):
            return
        return datetime(**dt)


    def prepare_line(self, line):
        return line


    def do_import(self, values):
        # First of all, parse the datetime
        values = self.prepare_line(values)
        date = self.datetime_parser(values)
        if not date:
            return
        ms = models.Measurement.objects.create(
            area=self.area, date=date, session=self.session,
        )
        for i in zip(self.csvmodel, values):
            if type(i[0]) == IgnoredColumn:
                continue
            models.MeasurementValue.objects.create(
                measurement=ms,
                property=i[0],
                value=float(i[1])
            )
