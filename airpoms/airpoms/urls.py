"""airpoms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from airpoms import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('i18n/', include('django.conf.urls.i18n')),
    path('', views.IndexView.as_view(), name='index'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('login/logout', views.LogoutView.as_view(), name='logout'),
    path('airpoms/about', views.AboutView.as_view(), name='about'),
    path('airpoms/map', views.MapView.as_view(), name='map'),
    path('airpoms/monitoredareas', views.MonitoredAreasList.as_view(), name='monitoredareas'),
    path('airpoms/monitoredareas/create', views.CreateMonitoredAreaView.as_view(), name='add-monitoredarea'),
    path('airpoms/area/<int:pk>', views.AreaView.as_view(), name='area'),
    path('airpoms/area/<int:pk>/session/create', views.CreateSessionView.as_view(), name='add-session'),
    path('airpoms/session/<int:pk>', views.SessionView.as_view(), name='session'),
    path('airpoms/session/<int:pk>/ajax', views.SessionDataAjax.as_view(), name='session-data'),
    path('airpoms/session/<int:pk>/drop_altitude', views.SessionDropAltitudeDataView.as_view(), name='session-drop-altitude'),
    path('airpoms/session/<int:pk>/graphs', views.SessionGraphsView.as_view(), name='session-graphs'),
    path('airpoms/session/<int:pk>/import', views.SessionImportDataView.as_view(), name='session-import-data'),
    path('airpoms/session/<int:pk>/import/ajax', views.SessionImportDataAjax.as_view(), name='session-import-ajax'),
    path('airpoms/session/<int:pk>/drop/selected', views.DropSelectedMeasurements.as_view(), name='session-drop-selected'),
    path('airpoms/session/<int:pk>/edit', views.EditSessionView.as_view(), name='session-edit'),
]
