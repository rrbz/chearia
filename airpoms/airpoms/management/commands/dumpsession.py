# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from airpoms.models import MeasurementSession
import csv


class Command(BaseCommand):
    help = "Dump an entire session to a csv file"


    def add_arguments(self, parser):
        parser.add_argument("sessid", type=int)


    def handle(self, sessid, *args, **opts):
        ms = MeasurementSession.objects.get(pk=sessid)
        intest_printed = False
        for i in ms.measurement_set.all():
            vals = []
            vals.append(i.date.strftime("%d,%m,%Y,%H,%M,%S"))
            for v in i.measurementvalue_set.all():
                vals.append(str(v.value))
            print(",".join(vals))
