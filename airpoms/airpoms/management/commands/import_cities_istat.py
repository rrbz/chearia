# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from airpoms.models import Country, Region, Province, City
import csv


class Command(BaseCommand):
    help = "Imports a CSV with Region, Province and City in a default country"


    def add_arguments(self, parser):
        parser.add_argument("country", type=str, default="it")
        parser.add_argument("fin", type=str)


    def handle(self, fin, country="it", *args, **opts):
        country = Country.objects.get(code=country)
        fh = open(fin)
        for i in csv.reader(fh):
            rg, state = Region.objects.get_or_create(name=i[2], country=country)
            pv, state = Province.objects.get_or_create(name=i[1], region=rg)
            ct, state = City.objects.get_or_create(name=i[0], province=pv)
            if state:
                print("Created", ct.name)
