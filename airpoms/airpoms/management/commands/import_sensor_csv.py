# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from airpoms import utils


class Command(BaseCommand):
    help = "Import a CSV file from the sensor"

    def add_arguments(self, parser):
        parser.add_argument("fname", type=str)
        parser.add_argument("area", type=str)
        parser.add_argument("session_name", type=str)

    def handle(self, fname=None, area=None, session_name=None, *args, **kw):
        print("Initializing...")
        handler = utils.AirPoMSCSVImporter(fname, area=area, session=session_name)
        print("Importing...")
        handler.run()
        print("Generating caches...")
        utils.generate_session_caches(handler.session)
        print("Done.")
