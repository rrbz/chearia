# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import StreamingHttpResponse
from django.views.generic import TemplateView, View, ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse_lazy
from django.shortcuts import redirect, render
from airpoms import forms, models, utils, strings
from airpoms.importer.fbk import FBKCSVImporter
import json
import collections


class IndexView(LoginRequiredMixin, View):
    def __init__(self, *args, **kw):
        self.post = self.get
        return super().__init__(*args, **kw)

    def get(self, request):
        return redirect("map")


class LoginView(FormView):
    template_name = "login.html"
    form_class = forms.LoginForm

    def form_valid(self, form):
        user = authenticate(
            self.request,
            username=form.cleaned_data["username"],
            password=form.cleaned_data["password"]
        )
        if user is not None:
            login(self.request, user)
            messages.success(self.request, _("Welcome, %s") % (user.get_full_name() or user.username))
        else:
            messages.warning(self.request, _("Username or password incorrect"))
            return self.render_to_response({"form": self.form_class})

        return redirect("map")


class LogoutView(LoginRequiredMixin, View):
    def __init__(self, *args, **kw):
        self.post = self.get
        return super().__init__(*args, **kw)

    def get(self, request):
        logout(request)
        return redirect("index")


class AboutView(LoginRequiredMixin, TemplateView):
    template_name = "airpoms/about.html"


class MapView(LoginRequiredMixin, TemplateView):
    template_name = "airpoms/map.html"

    def get_context_data(self):
        ctx = {}
        areas = []
        for a in models.MonitoredArea.objects.all():
            this = utils.get_last_area_data(a)
            if this:
                areas.append(this)
        ctx["areas"] = areas
        return ctx


class MonitoredAreasList(LoginRequiredMixin, ListView):
    model = models.MonitoredArea
    template_name = "airpoms/areas.html"


class CreateMonitoredAreaView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = models.MonitoredArea
    fields = ["name", "city"]
    template_name = "airpoms/generic_formview.html"
    success_url = reverse_lazy("monitoredareas")
    permission_required = ["airpoms.add_monitoredarea"]
    extra_context = {
        "title": _("Create monitored area"),
        "submit_text": _("Save"),
        "btn": "btn-success",
        "fa": "fa-save"
    }


class AreaView(LoginRequiredMixin, DetailView):
    model = models.MonitoredArea
    template_name = "airpoms/area.html"

    def get_context_data(self, object):
        ctx = super().get_context_data()
        ctx["measurements"] = utils.get_all_measurements_data(object)
        return ctx


class CreateSessionView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = models.MeasurementSession
    fields = ["name"]
    template_name = "airpoms/generic_formview.html"
    permission_required = ["airpoms.add_measurementsession"]
    extra_context = {
        "title": _("Create measurement session"),
        "submit_text": _("Save"),
        "btn": "btn-success",
        "fa": "fa-save"
    }

    def get_success_url(self):
        return reverse_lazy("session", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        form.instance.area = models.MonitoredArea.objects.get(pk=self.kwargs["pk"])
        return super().form_valid(form)


class SessionView(LoginRequiredMixin, DetailView):
    model = models.MeasurementSession
    template_name = "airpoms/session.html"


class SessionDataAjax(LoginRequiredMixin, View):
    def data_generator(self, queryset):
        for i in queryset:
            values = collections.OrderedDict()
            values["Date"] = [i.date.strftime("%d/%m/%Y %H:%M:%S"), "", i.date.timestamp()*1000]
            for e in i.measurementvalue_set.all().order_by("property"):
                values[e.property.name] = [e.value, e.property.unit or ""]
            values["qc_norender"] = i.qualitycolor()
            values["id_norender"] = i.pk
            yield json.dumps(values) + "\n"

    def get(self, *args, **kw):
        qs = models.MeasurementSession.objects.get(pk=kw["pk"]).measurement_set.all()
        return StreamingHttpResponse(self.data_generator(qs))


class SessionDropAltitudeDataView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    permission_required = ["airpoms.delete_measurementvalue"]
    template_name = "airpoms/generic_formview.html"

    def get_context_data(self, pk):
        ctx = super().get_context_data()
        self.object = models.MeasurementSession.objects.get(pk=pk)
        ctx["title"] = _("Really delete %s altitude data?") % self.object.name
        ctx["submit_text"] = _("Delete")
        ctx["btn"] = "btn-danger"
        ctx["fa"] = "fa-trash"
        return ctx

    def post(self, *args, **kw):
        p = models.Property.objects.get(name="Altitude")
        self.object = models.MeasurementSession.objects.get(pk=kw["pk"])
        for i in self.object.measurement_set.all():
            i.measurementvalue_set.filter(property=p).delete()
        return redirect("session", pk=kw["pk"])


class SessionGraphsView(LoginRequiredMixin, TemplateView):
    template_name = "airpoms/graphs.html"

    def get_context_data(self, pk):
        ctx = super().get_context_data()
        self.object = models.MeasurementSession.objects.get(pk=pk)
        ctx["object"] = self.object
        ctx["totalsamples"] = len(self.object.measurement_set.all())
        ctx["props"] = models.Property.objects.all()
        return ctx


class SessionImportDataView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    template_name = "airpoms/importer.html"
    form_class = forms.ImportForm
    permission_required = ["airpoms.add_measurement"]
    extra_context = {
        "title": _("Import measurements from file"),
        "submit_text": _("Import"),
        "btn": "btn-success",
        "fa": "fa-cloud-upload-alt"
    }

    def get_context_data(self):
        ctx = super().get_context_data()
        ctx["pk"] = self.kwargs["pk"]
        return ctx


class SessionImportDataAjax(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = ["airpoms.add_measurement"]

    def post(self, request, pk, *args, **kwargs):
        # Resolve the session
        session = models.MeasurementSession.objects.get(pk=pk)
        #print(request.POST, request.FILES)
        content = list(request.FILES.values())[0].readlines()
        content = [a.decode("utf-8") for a in content]
        # Initialize the importer
        if request.POST["datatype"] == "airpoms.csv":
            imp = utils.AirPoMSCSVImporter(
                content=content,
                session=session
            )
        elif request.POST["datatype"] == "info.txt":
            imp = FBKCSVImporter(
                content=content,
                session=session
            )
        return StreamingHttpResponse(imp.as_generator(use_json=True))


class DropSelectedMeasurements(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = ["airpoms.add_measurement"]

    def populate(self, ids):
        self.measurements = []
        for id in ids:
            if id not in ["csrfmiddlewaretoken", "dodelete"]:
                this = models.Measurement.objects.get(pk=id)
                if this.session_id != self.pk:
                    raise PermissionDenied
                self.measurements.append(this)

    def post(self, request, pk, *args, **kw):
        self.pk = pk
        self.populate(request.POST.keys())
        if "dodelete" in request.POST:
            for i in self.measurements:
                i.delete()
            return redirect("session", pk=pk)
        return render(request, "airpoms/dropselected.html", {"ms": self.measurements})


class EditSessionView(LoginRequiredMixin, PermissionDenied, UpdateView):
    permission_required = ["airpoms.change_measurementsession"]
    model = models.MeasurementSession
    fields = ["name", "notes"]
    template_name = "airpoms/editsession.html"

    def get_success_url(self):
        return reverse_lazy("session", kwargs={"pk": self.object.pk})
