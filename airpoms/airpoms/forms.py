# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': "form-control", "placeholder": _("Username")}
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': "form-control", "placeholder": _("Password")}
        )
    )


class ImportForm(forms.Form):
    datatypes = [
        ["airpoms.csv", _("AirPoMS sensor comma separted value file (AIRPOMS.CSV)")],
        ["info.txt", _("FBK sensor built-in file (INFO.TXT)")],
    ]
    datatype = forms.ChoiceField(choices=datatypes, label=_("Data type"))
    file = forms.FileField(label=_("File"))
    altitude = forms.BooleanField(required=False, label=_("Import altitude data"))
