from django import template

register = template.Library()


def getproperty(qs, name):
    try:
        return qs.get(property__name=name).value
    except:
        return None


register.filter("getproperty", getproperty)
