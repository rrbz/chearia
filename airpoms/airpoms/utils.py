# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@school.rainerum.it>
# This code has been written for Rainerum Robotic's project "cheAria" in
# collaboration with FBK Junior
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
from datetime import datetime
from airpoms import models
from django.db.models import Avg
import json
from airpoms.importer import generate_session_caches, AirPoMSCSVImporter


def iaq_color(val):
    if val <= 50:
        return "#219807"
    elif 50 < val <= 100:
        return "#B0D502"
    elif 100 < val <= 150:
        return "#FCE202"
    elif 150 < val <= 200:
        return "#FB8801"
    elif 200 < val <= 300:
        return "#CA0C73"
    return "#5B0187"


class DataArea:
    def __init__(self, cache):
        for i in cache:
            setattr(self, i.property.name.lower(), i.value)
            if i.property.name.lower() == "iaq":
                self.qualitycolor = iaq_color(i.value)


    def set_date(self, date):
        self.date = date
        self.jstimestamp = date.timestamp()*1000


def get_last_area_data(area):
    # Gather the last (valid) measurement
    ms = None
    for e in area.measurement_set.order_by("-date"):
        cache = e.session.measurementsessioncache_set.all()
        if len(cache):
            ms = e
            break
    if not ms:
        return None
    obj = DataArea(cache)
    obj.set_date(ms.date)
    obj.area = area
    obj.session = ms.session
    return obj


def get_date(obj):
    return obj.date


def get_all_measurements_data(area):
    qs = models.MeasurementSession.objects.filter(area=area)
    all = []
    for i in qs:
        nqs = i.measurementsessioncache_set.all()
        obj = DataArea(nqs)
        try:
            obj.set_date(nqs[0].session.measurement_set.all()[0].date)
        except:
            obj.set_date(datetime.now())
        obj.session = i
        obj.area = i.area
        all.append(obj)
    return list(sorted(all, key=get_date, reverse=True))
