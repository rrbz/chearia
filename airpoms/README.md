# AirPoMS

## Stands for Air Pollution Monitoring Software

Is based on Django 2.1.5 and Python 3.


## Quickstarter guide

* Clone this repository;
* Make sure you have Python 3.X and Django installed;
* Run `python3 manage.py initialize_properties`;
* OPTIONAL but highly suggested: download and import the cities of your country with `python3 manage.py import_cities_istat`;
* Compile i18n stuff with `python3 manage.py compilemessages` (make sure gettext is installed);
* Start the *debug* server with `python3 manage.py runserver`;
* Enjoy!
