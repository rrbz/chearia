# Che Aria!

## A project in collaboration with FBK Junior.

Air pollution control software and hardware.


## What's in this repository?

Basically, two directories:

* `GasMapper2RR`: Modified version of the sensor firmware, maintained by @j54n1n ;
* `airpoms`: _Air Pollution Monitoring Software_, a WebApp to process data incoming from sensors, maintained by @mmaridev .
